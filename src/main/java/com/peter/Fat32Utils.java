package com.peter;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

import com.peterswing.CommonLib;

import de.waldheinz.fs.FsDirectoryEntry;
import de.waldheinz.fs.FsFile;
import de.waldheinz.fs.fat.FatFileSystem;
import de.waldheinz.fs.fat.FatLfnDirectory;
import de.waldheinz.fs.fat.FatLfnDirectoryEntry;
import de.waldheinz.fs.fat.FatType;
import de.waldheinz.fs.fat.SuperFloppyFormatter;
import de.waldheinz.fs.util.FileDisk;

public class Fat32Utils {
	public static CommandLine cmd;

	public static void main(String args[]) {
		CommandLineParser parser = new PosixParser();
		Options options = new Options();
		try {
			options.addOption(OptionBuilder.withArgName("file size").withValueSeparator(' ').hasArgs(2).withDescription("create harddisk <file> <size>").create("c"));
			options.addOption(OptionBuilder.withArgName("file offset").withValueSeparator(' ').hasArgs(2).withDescription("mkfs.fat <file> <offset>").create("m"));
			cmd = parser.parse(options, args);
		} catch (ParseException e1) {
			e1.printStackTrace();
			System.exit(1);
		}

		try {
			if (cmd.hasOption("c")) {
				String[] optionValues = cmd.getOptionValues("c");
				String filename = optionValues[0];
				long size = CommonLib.convertFilesize(optionValues[1]);

				RandomAccessFile f = new RandomAccessFile(filename, "rw");
				f.setLength(size);
				f.close();
				System.out.println("created " + filename);
			} else if (cmd.hasOption("m")) {
				String[] optionValues = cmd.getOptionValues("m");
				String filename = optionValues[0];
				long offset = CommonLib.convertFilesize(optionValues[1]);

//				File file = new File(filename);
				RandomAccessFile file = new RandomAccessFile(filename, "rw");
				file.seek(1000);
				//				FileDisk dev = FileDisk.create(file, 40 * 1024 * 1024);
				FileDisk dev = new FileDisk(file, false);
				SuperFloppyFormatter sf = SuperFloppyFormatter.get(dev);
				sf.setOemName("Peter32");
				sf.setVolumeLabel("Peter Fat32");
				sf.setFatType(FatType.FAT32);
				sf.format();

				FatFileSystem fs = new FatFileSystem(dev, false);
				final FatLfnDirectory rootDir = (FatLfnDirectory) fs.getRoot();
				FatLfnDirectoryEntry entry = rootDir.addDirectory("Directory");
				for (int i = 0; i < 10; i++) {
					final FsDirectoryEntry e = entry.getDirectory().addFile("file_" + i);
					final FsFile fsFile = e.getFile();

					byte[] nullBytes = "Peter 你好 UTF8\n".getBytes();
					ByteBuffer buff = ByteBuffer.wrap(nullBytes);
					buff.rewind();
					fsFile.write(0, buff);
				}
				fs.flush();
				fs.close();
				System.out.println("done");
			}
			/*
				File file = new File("peter.img");
				if (!file.delete()) {
					System.out.println("unable to delete " + file.getName());
				}
				FileDisk dev = FileDisk.create(file, 40 * 1024 * 1024);
				SuperFloppyFormatter sf = SuperFloppyFormatter.get(dev);
				sf.setOemName("Peter32");
				sf.setVolumeLabel("Peter Fat32");
				sf.setFatType(FatType.FAT32);
				sf.format();

				FatFileSystem fs = new FatFileSystem(dev, false);
				final FatLfnDirectory rootDir = (FatLfnDirectory) fs.getRoot();
				FatLfnDirectoryEntry entry = rootDir.addDirectory("Directory");
				for (int i = 0; i < 10; i++) {
					final FsDirectoryEntry e = entry.getDirectory().addFile("file_" + i);
					final FsFile fsFile = e.getFile();

					byte[] nullBytes = "Peter 你好 UTF8\n".getBytes();
					ByteBuffer buff = ByteBuffer.wrap(nullBytes);
					buff.rewind();
					fsFile.write(0, buff);
				}
				fs.flush();
				fs.close();
			System.out.println("done");*/

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
